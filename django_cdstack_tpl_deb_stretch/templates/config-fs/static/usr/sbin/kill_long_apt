#!/bin/bash

# Maximum runtime in seconds (24 hours = 86400 seconds)
MAX_RUNTIME=$((24 * 60 * 60))

# Iterate through all running dpkg, apt, apt-get, and aptitude processes
for process in dpkg apt apt-get aptitude; do
    while IFS= read -r pid; do
        # If there are no matching processes, skip
        [[ -z "$pid" ]] && continue
        
        # Retrieve the start time of the process
        start_time=$(ps -o etimes= -p "$pid" 2>/dev/null)
        
        # If no start time is found, skip
        [[ -z "$start_time" ]] && continue
        
        # If the process has been running longer than MAX_RUNTIME, terminate it
        if (( start_time > MAX_RUNTIME )); then
            echo "Terminating $process process with PID $pid (running for $start_time seconds)"
            kill "$pid"
        fi
    done < <(pgrep -x "$process")
done
