import copy
import ipaddress
import os

import yaml
from django.template import engines

from django_cdstack_deploy.django_cdstack_deploy.func import (
    generate_config_static,
    zip_add_file,
    get_host_vars,
    generate_motd,
    extract_hostname,
)

from django_cdstack_tpl_icinga2_client.django_cdstack_tpl_icinga2_client.views import (
    handle as handle_icinga2_client,
)


def lookup_vars(template_opts, net_iface, var_in, var_out):
    if var_in in template_opts:
        net_iface[var_out] = template_opts[var_in]
    return


def list_remove_duplicates(x):
    return list(dict.fromkeys(x))


def clean_new_lines(string: str):
    cleaned_string = str()

    for line in string.splitlines():
        if line.strip() not in os.linesep:
            if line.startswith("iface"):
                cleaned_string += "\n" + line + "\n"
            else:
                cleaned_string += line + "\n"

    return cleaned_string


def get_apt_packages(image_yaml_path: str):
    apt_packages = list()

    with open(image_yaml_path, "rt") as stream:
        file_content = yaml.safe_load(stream)

    apt_packages += file_content["bootstrapper"]["include_packages"]
    apt_packages += file_content["packages"]["install"]

    apt_packages = list_remove_duplicates(apt_packages)

    return apt_packages


def get_apt_sources(image_yaml_path: str):
    with open(image_yaml_path, "rt") as stream:
        file_content = yaml.safe_load(stream)

    apt_sources = file_content["packages"]["sources"]

    return apt_sources


def get_apt_prefs(image_yaml_path: str):
    with open(image_yaml_path, "rt") as stream:
        file_content = yaml.safe_load(stream)

    apt_preferences = dict()

    if "preferences" in file_content["packages"]:
        apt_preferences = file_content["packages"]["preferences"]

    return apt_preferences


def handle(zipfile_handler, template_opts, cmdb_host, single_network_file=False):
    django_engine = engines["django"]

    module_prefix = "django_cdstack_tpl_deb_stretch/django_cdstack_tpl_deb_stretch"

    template_opts["single_network_file"] = single_network_file
    template_opts["single_network_file_content"] = str()

    if "apt_packages" not in template_opts:
        template_opts["apt_packages"] = get_apt_packages(
            module_prefix + "/templates/image/imageconf/vmtemplate.yml"
        )

    if "fail2ban_apps" not in template_opts:
        template_opts["fail2ban_apps"] = copy.deepcopy(template_opts["apt_packages"])

    if "apt_sources" not in template_opts:
        template_opts["apt_sources"] = get_apt_sources(
            module_prefix + "/templates/image/imageconf/vmtemplate.yml"
        )

    if "apt_preferences" not in template_opts:
        template_opts["apt_preferences"] = get_apt_prefs(
            module_prefix + "/templates/image/imageconf/vmtemplate.yml"
        )

    if "os_release" not in template_opts:
        template_opts["os_release"] = get_os_release(
            module_prefix + "/templates/image/imageconf/vmtemplate.yml"
        )

    if "apt_packages_purge" not in template_opts:
        template_opts["apt_packages_purge"] = list()

    if "network_iface_primary_ip" not in template_opts:
        if "network_iface_eth0_ip" in template_opts:
            template_opts["network_iface_primary_ip"] = template_opts[
                "network_iface_eth0_ip"
            ]

    if "network_iface_primary_ip6" not in template_opts:
        if "network_iface_eth0_ip6" in template_opts:
            template_opts["network_iface_primary_ip6"] = template_opts[
                "network_iface_eth0_ip6"
            ]

    # Remove old puppet agent if new is requested
    if "puppet-agent" in template_opts["apt_packages"]:
        template_opts["apt_packages_purge"].append("puppet")

    # Remove other firewalls if shorewall is requested
    if "shorewall" in template_opts["apt_packages"]:
        template_opts["apt_packages_purge"].append("ufw")
        template_opts["apt_packages_purge"].append("ferm")

    # Remove squid if squid-openssl is requested
    if "squid-openssl" in template_opts["apt_packages"]:
        template_opts["apt_packages_purge"].append("squid")

    # Remove squid-openssl if squid is requested
    if "squid" in template_opts["apt_packages"]:
        template_opts["apt_packages_purge"].append("squid-openssl")

    template_opts["other_hosts_in_group"] = list()

    if len(cmdb_host.cmdbhostgroups_set.filter(apply_order=0)) > 0:
        primary_group = cmdb_host.cmdbhostgroups_set.get(apply_order=0).group_rel

        for other_cmdb_host_assignment in primary_group.cmdbhostgroups_set.exclude(
            host_rel=cmdb_host
        ).all():
            template_opts["other_hosts_in_group"].append(
                get_host_vars(other_cmdb_host_assignment.host_rel)
            )

    if "pppoe_connections" not in template_opts:
        template_opts["pppoe_connections"] = list()

    pppoe_connections = list()

    net_ifaces_eth = list()

    for key in template_opts.keys():
        if key.startswith("network_iface_eth") and key.endswith("_hwaddr"):
            key_ident = key[:-7]

            network_iface_split = key.split("_")
            network_iface = network_iface_split[2]

            net_iface = dict()
            net_iface["network_iface"] = network_iface

            if "network_general_private_gw" in template_opts:
                internal_network = ipaddress.ip_network(
                    template_opts[key_ident + "_ip"]
                    + "/"
                    + template_opts[key_ident + "_netmask"],
                    strict=False,
                )

                if (
                    ipaddress.ip_address(template_opts["network_general_private_gw"])
                    in internal_network
                ):
                    net_iface["network_iface_to_private_gw"] = True
                else:
                    net_iface["network_iface_to_private_gw"] = False

            lookup_vars(
                template_opts, net_iface, key_ident + "_mode", "network_iface_eth_mode"
            )
            lookup_vars(
                template_opts,
                net_iface,
                key_ident + "_mode6",
                "network_iface_eth_mode6",
            )
            lookup_vars(
                template_opts, net_iface, key_ident + "_ip", "network_iface_eth_ip"
            )
            lookup_vars(
                template_opts,
                net_iface,
                key_ident + "_netmask",
                "network_iface_eth_netmask",
            )
            lookup_vars(
                template_opts,
                net_iface,
                key_ident + "_hwaddr",
                "network_iface_eth_hwaddr",
            )
            lookup_vars(
                template_opts, net_iface, key_ident + "_mtu", "network_iface_eth_mtu"
            )
            lookup_vars(
                template_opts, net_iface, key_ident + "_gw", "network_iface_eth_gw"
            )
            lookup_vars(
                template_opts, net_iface, key_ident + "_ptp", "network_iface_eth_ptp"
            )
            lookup_vars(
                template_opts, net_iface, key_ident + "_ip6", "network_iface_eth_ip6"
            )
            lookup_vars(
                template_opts,
                net_iface,
                key_ident + "_netmask6",
                "network_iface_eth_netmask6",
            )
            lookup_vars(
                template_opts, net_iface, key_ident + "_gw6", "network_iface_eth_gw6"
            )
            lookup_vars(
                template_opts, net_iface, key_ident + "_pre_up", "network_iface_pre_up"
            )
            lookup_vars(
                template_opts,
                net_iface,
                key_ident + "_post_up",
                "network_iface_post_up",
            )
            lookup_vars(
                template_opts,
                net_iface,
                key_ident + "_pppoe_username",
                "network_iface_pppoe_username",
            )
            lookup_vars(
                template_opts,
                net_iface,
                key_ident + "_pppoe_password",
                "network_iface_pppoe_password",
            )
            lookup_vars(
                template_opts,
                net_iface,
                key_ident + "_ppp_unit",
                "network_iface_ppp_unit",
            )

            if "network_iface_pppoe_username" in net_iface:
                pppoe_connection = dict()
                pppoe_connection["network_pppoe_username"] = net_iface[
                    "network_iface_pppoe_username"
                ]
                pppoe_connection["network_pppoe_password"] = net_iface[
                    "network_iface_pppoe_password"
                ]
                pppoe_connection["network_pppoe_iface"] = net_iface["network_iface"]

                if "network_iface_ppp_unit" not in net_iface:
                    net_iface["network_iface_ppp_unit"] = "0"

                pppoe_connection["network_iface_ppp_unit"] = net_iface[
                    "network_iface_ppp_unit"
                ]

                pppoe_connections.append(pppoe_connection)

            merged_opts = {**template_opts, **net_iface}

            config_template_file = open(
                module_prefix
                + "/templates/config-fs/dynamic/etc/network/interfaces.d/eth.cfg",
                "r",
            ).read()
            config_template = django_engine.from_string(config_template_file)

            zip_add_file(
                zipfile_handler,
                "etc/network/interfaces.d/" + net_iface["network_iface"] + ".cfg",
                clean_new_lines(config_template.render(merged_opts)),
            )

            template_opts["single_network_file_content"] += (
                clean_new_lines(config_template.render(merged_opts)) + "\n\n"
            )

            if "network_iface_pre_up" in merged_opts:
                config_template_file = open(
                    module_prefix
                    + "/templates/config-fs/dynamic/etc/network/scripts/pre-up.sh",
                    "r",
                ).read()
                config_template = django_engine.from_string(config_template_file)

                zip_add_file(
                    zipfile_handler,
                    "etc/network/scripts/" + network_iface + "/pre-up.sh",
                    clean_new_lines(config_template.render(merged_opts)),
                )

            if "network_iface_post_up" in merged_opts:
                config_template_file = open(
                    module_prefix
                    + "/templates/config-fs/dynamic/etc/network/scripts/post-up.sh",
                    "r",
                ).read()
                config_template = django_engine.from_string(config_template_file)

                zip_add_file(
                    zipfile_handler,
                    "etc/network/scripts/" + network_iface + "/post-up.sh",
                    clean_new_lines(config_template.render(merged_opts)),
                )

            if "network_iface_pppoe_username" in merged_opts:
                config_template_file = open(
                    module_prefix + "/templates/config-fs/dynamic/etc/ppp/peers/peer",
                    "r",
                ).read()
                config_template = django_engine.from_string(config_template_file)

                zip_add_file(
                    zipfile_handler,
                    "etc/ppp/peers/" + network_iface,
                    config_template.render(merged_opts),
                )

            net_ifaces_eth.append(net_iface)

        if (
            key.startswith("network_iface_br") or key.startswith("network_iface_vmbr")
        ) and key.endswith("_hwaddr"):
            key_ident = key[:-7]

            network_iface_split = key.split("_")
            network_iface = network_iface_split[2]

            net_iface = dict()
            net_iface["network_iface"] = network_iface

            if "network_general_private_gw" in template_opts:
                internal_network = ipaddress.ip_network(
                    template_opts[key_ident + "_ip"]
                    + "/"
                    + template_opts[key_ident + "_netmask"],
                    strict=False,
                )

                if (
                    ipaddress.ip_address(template_opts["network_general_private_gw"])
                    in internal_network
                ):
                    net_iface["network_iface_to_private_gw"] = True
                else:
                    net_iface["network_iface_to_private_gw"] = False

            lookup_vars(
                template_opts, net_iface, key_ident + "_mode", "network_iface_br_mode"
            )
            lookup_vars(
                template_opts, net_iface, key_ident + "_mode6", "network_iface_br_mode6"
            )
            lookup_vars(
                template_opts, net_iface, key_ident + "_ports", "network_iface_br_ports"
            )
            lookup_vars(
                template_opts,
                net_iface,
                key_ident + "_vlan_aware",
                "network_iface_br_vlan_aware",
            )
            lookup_vars(
                template_opts, net_iface, key_ident + "_vids", "network_iface_br_vids"
            )
            lookup_vars(
                template_opts, net_iface, key_ident + "_ip", "network_iface_br_ip"
            )
            lookup_vars(
                template_opts,
                net_iface,
                key_ident + "_netmask",
                "network_iface_br_netmask",
            )
            lookup_vars(
                template_opts,
                net_iface,
                key_ident + "_hwaddr",
                "network_iface_br_hwaddr",
            )
            lookup_vars(
                template_opts, net_iface, key_ident + "_mtu", "network_iface_br_mtu"
            )
            lookup_vars(
                template_opts, net_iface, key_ident + "_gw", "network_iface_br_gw"
            )
            lookup_vars(
                template_opts, net_iface, key_ident + "_ptp", "network_iface_br_ptp"
            )
            lookup_vars(
                template_opts, net_iface, key_ident + "_ip6", "network_iface_br_ip6"
            )
            lookup_vars(
                template_opts,
                net_iface,
                key_ident + "_netmask6",
                "network_iface_br_netmask6",
            )
            lookup_vars(
                template_opts, net_iface, key_ident + "_gw6", "network_iface_br_gw6"
            )
            lookup_vars(
                template_opts, net_iface, key_ident + "_pre_up", "network_iface_pre_up"
            )
            lookup_vars(
                template_opts,
                net_iface,
                key_ident + "_post_up",
                "network_iface_post_up",
            )

            merged_opts = {**template_opts, **net_iface}

            config_template_file = open(
                module_prefix
                + "/templates/config-fs/dynamic/etc/network/interfaces.d/br.cfg",
                "r",
            ).read()
            config_template = django_engine.from_string(config_template_file)

            zip_add_file(
                zipfile_handler,
                "etc/network/interfaces.d/" + net_iface["network_iface"] + ".cfg",
                clean_new_lines(config_template.render(merged_opts)),
            )

            template_opts["single_network_file_content"] += (
                clean_new_lines(config_template.render(merged_opts)) + "\n\n"
            )

            if "network_iface_pre_up" in merged_opts:
                config_template_file = open(
                    module_prefix
                    + "/templates/config-fs/dynamic/etc/network/scripts/pre-up.sh",
                    "r",
                ).read()
                config_template = django_engine.from_string(config_template_file)

                zip_add_file(
                    zipfile_handler,
                    "etc/network/scripts/" + network_iface + "/pre-up.sh",
                    clean_new_lines(config_template.render(merged_opts)),
                )

            if "network_iface_post_up" in merged_opts:
                config_template_file = open(
                    module_prefix
                    + "/templates/config-fs/dynamic/etc/network/scripts/post-up.sh",
                    "r",
                ).read()
                config_template = django_engine.from_string(config_template_file)

                zip_add_file(
                    zipfile_handler,
                    "etc/network/scripts/" + network_iface + "/post-up.sh",
                    clean_new_lines(config_template.render(merged_opts)),
                )

        if key.startswith("network_iface_bond") and key.endswith("_hwaddr"):
            key_ident = key[:-7]

            network_iface_split = key.split("_")
            network_iface = network_iface_split[2]

            net_iface = dict()
            net_iface["network_iface"] = network_iface

            if "network_general_private_gw" in template_opts:
                internal_network = ipaddress.ip_network(
                    template_opts[key_ident + "_ip"]
                    + "/"
                    + template_opts[key_ident + "_netmask"],
                    strict=False,
                )

                if (
                    ipaddress.ip_address(template_opts["network_general_private_gw"])
                    in internal_network
                ):
                    net_iface["network_iface_to_private_gw"] = True
                else:
                    net_iface["network_iface_to_private_gw"] = False

            lookup_vars(
                template_opts, net_iface, key_ident + "_mode", "network_iface_bond_mode"
            )
            lookup_vars(
                template_opts,
                net_iface,
                key_ident + "_mode6",
                "network_iface_bond_mode6",
            )
            lookup_vars(
                template_opts,
                net_iface,
                key_ident + "_slaves",
                "network_iface_bond_slaves",
            )
            lookup_vars(
                template_opts,
                net_iface,
                key_ident + "_miimon",
                "network_iface_bond_miimon",
            )
            lookup_vars(
                template_opts,
                net_iface,
                key_ident + "_bond_mode",
                "network_iface_bond_bond_mode",
            )
            lookup_vars(
                template_opts,
                net_iface,
                key_ident + "_xmit_hash_policy",
                "network_iface_bond_xmit_hash_policy",
            )
            lookup_vars(
                template_opts, net_iface, key_ident + "_ip", "network_iface_bond_ip"
            )
            lookup_vars(
                template_opts,
                net_iface,
                key_ident + "_netmask",
                "network_iface_bond_netmask",
            )
            lookup_vars(
                template_opts,
                net_iface,
                key_ident + "_hwaddr",
                "network_iface_bond_hwaddr",
            )
            lookup_vars(
                template_opts, net_iface, key_ident + "_mtu", "network_iface_bond_mtu"
            )
            lookup_vars(
                template_opts, net_iface, key_ident + "_gw", "network_iface_bond_gw"
            )
            lookup_vars(
                template_opts, net_iface, key_ident + "_ptp", "network_iface_bond_ptp"
            )
            lookup_vars(
                template_opts, net_iface, key_ident + "_ip6", "network_iface_bond_ip6"
            )
            lookup_vars(
                template_opts,
                net_iface,
                key_ident + "_netmask6",
                "network_iface_bond_netmask6",
            )
            lookup_vars(
                template_opts, net_iface, key_ident + "_gw6", "network_iface_bond_gw6"
            )
            lookup_vars(
                template_opts, net_iface, key_ident + "_pre_up", "network_iface_pre_up"
            )
            lookup_vars(
                template_opts,
                net_iface,
                key_ident + "_post_up",
                "network_iface_post_up",
            )

            merged_opts = {**template_opts, **net_iface}

            config_template_file = open(
                module_prefix
                + "/templates/config-fs/dynamic/etc/network/interfaces.d/bond.cfg",
                "r",
            ).read()
            config_template = django_engine.from_string(config_template_file)

            zip_add_file(
                zipfile_handler,
                "etc/network/interfaces.d/" + net_iface["network_iface"] + ".cfg",
                clean_new_lines(config_template.render(merged_opts)),
            )

            template_opts["single_network_file_content"] += (
                clean_new_lines(config_template.render(merged_opts)) + "\n\n"
            )

            if "network_iface_pre_up" in merged_opts:
                config_template_file = open(
                    module_prefix
                    + "/templates/config-fs/dynamic/etc/network/scripts/pre-up.sh",
                    "r",
                ).read()
                config_template = django_engine.from_string(config_template_file)

                zip_add_file(
                    zipfile_handler,
                    "etc/network/scripts/" + network_iface + "/pre-up.sh",
                    clean_new_lines(config_template.render(merged_opts)),
                )

            if "network_iface_post_up" in merged_opts:
                config_template_file = open(
                    module_prefix
                    + "/templates/config-fs/dynamic/etc/network/scripts/post-up.sh",
                    "r",
                ).read()
                config_template = django_engine.from_string(config_template_file)

                zip_add_file(
                    zipfile_handler,
                    "etc/network/scripts/" + network_iface + "/post-up.sh",
                    clean_new_lines(config_template.render(merged_opts)),
                )

        if (
            key.startswith("network_iface_ovsbr")
            or key.startswith("network_iface_vmovsbr")
        ) and key.endswith("_hwaddr"):
            key_ident = key[:-7]

            network_iface_split = key.split("_")
            network_iface = network_iface_split[2]

            net_iface = dict()
            net_iface["network_iface"] = network_iface

            if network_iface.startswith("vmovsbr"):
                net_iface["network_iface"] = network_iface.replace("vmovsbr", "vmbr")

            if "network_general_private_gw" in template_opts:
                internal_network = ipaddress.ip_network(
                    template_opts[key_ident + "_ip"]
                    + "/"
                    + template_opts[key_ident + "_netmask"],
                    strict=False,
                )

                if (
                    ipaddress.ip_address(template_opts["network_general_private_gw"])
                    in internal_network
                ):
                    net_iface["network_iface_to_private_gw"] = True
                else:
                    net_iface["network_iface_to_private_gw"] = False

            lookup_vars(
                template_opts, net_iface, key_ident + "_mode", "network_iface_br_mode"
            )
            lookup_vars(
                template_opts, net_iface, key_ident + "_mode6", "network_iface_br_mode6"
            )
            lookup_vars(
                template_opts, net_iface, key_ident + "_ports", "network_iface_br_ports"
            )
            lookup_vars(
                template_opts, net_iface, key_ident + "_ip", "network_iface_br_ip"
            )
            lookup_vars(
                template_opts,
                net_iface,
                key_ident + "_netmask",
                "network_iface_br_netmask",
            )
            lookup_vars(
                template_opts,
                net_iface,
                key_ident + "_hwaddr",
                "network_iface_br_hwaddr",
            )
            lookup_vars(
                template_opts, net_iface, key_ident + "_mtu", "network_iface_br_mtu"
            )
            lookup_vars(
                template_opts, net_iface, key_ident + "_gw", "network_iface_br_gw"
            )
            lookup_vars(
                template_opts, net_iface, key_ident + "_ptp", "network_iface_br_ptp"
            )
            lookup_vars(
                template_opts, net_iface, key_ident + "_ip6", "network_iface_br_ip6"
            )
            lookup_vars(
                template_opts,
                net_iface,
                key_ident + "_netmask6",
                "network_iface_br_netmask6",
            )
            lookup_vars(
                template_opts, net_iface, key_ident + "_gw6", "network_iface_br_gw6"
            )
            lookup_vars(
                template_opts, net_iface, key_ident + "_pre_up", "network_iface_pre_up"
            )
            lookup_vars(
                template_opts,
                net_iface,
                key_ident + "_post_up",
                "network_iface_post_up",
            )

            if "network_iface_br_ports" in net_iface:
                net_iface["network_iface_br_ports_list"] = net_iface[
                    "network_iface_br_ports"
                ].split(" ")
            else:
                net_iface["network_iface_br_ports_list"] = list()

            merged_opts = {**template_opts, **net_iface}

            config_template_file = open(
                module_prefix
                + "/templates/config-fs/dynamic/etc/network/interfaces.d/ovsbr.cfg",
                "r",
            ).read()
            config_template = django_engine.from_string(config_template_file)

            zip_add_file(
                zipfile_handler,
                "etc/network/interfaces.d/" + net_iface["network_iface"] + ".cfg",
                clean_new_lines(config_template.render(merged_opts)),
            )

            template_opts["single_network_file_content"] += (
                clean_new_lines(config_template.render(merged_opts)) + "\n\n"
            )

            for network_bridge_port in net_iface["network_iface_br_ports_list"]:
                config_template_file = open(
                    module_prefix
                    + "/templates/config-fs/dynamic/etc/network/interfaces.d/ovsbr_port.cfg",
                    "r",
                ).read()
                config_template = django_engine.from_string(config_template_file)

                merged_opts["network_bridge_port"] = network_bridge_port

                zip_add_file(
                    zipfile_handler,
                    "etc/network/interfaces.d/" + network_bridge_port + ".cfg",
                    clean_new_lines(config_template.render(merged_opts)),
                )

                template_opts["single_network_file_content"] += (
                    clean_new_lines(config_template.render(merged_opts)) + "\n\n"
                )

            if "network_iface_pre_up" in merged_opts:
                config_template_file = open(
                    module_prefix
                    + "/templates/config-fs/dynamic/etc/network/scripts/pre-up.sh",
                    "r",
                ).read()
                config_template = django_engine.from_string(config_template_file)

                zip_add_file(
                    zipfile_handler,
                    "etc/network/scripts/" + network_iface + "/pre-up.sh",
                    clean_new_lines(config_template.render(merged_opts)),
                )

            if "network_iface_post_up" in merged_opts:
                config_template_file = open(
                    module_prefix
                    + "/templates/config-fs/dynamic/etc/network/scripts/post-up.sh",
                    "r",
                ).read()
                config_template = django_engine.from_string(config_template_file)

                zip_add_file(
                    zipfile_handler,
                    "etc/network/scripts/" + network_iface + "/post-up.sh",
                    clean_new_lines(config_template.render(merged_opts)),
                )

        if key.startswith("network_iface_bat") and key.endswith("_hwaddr"):
            key_ident = key[:-7]

            network_iface_split = key.split("_")
            network_iface = network_iface_split[2]

            net_iface = dict()
            net_iface["network_iface"] = network_iface

            if "network_general_private_gw" in template_opts:
                internal_network = ipaddress.ip_network(
                    template_opts[key_ident + "_ip"]
                    + "/"
                    + template_opts[key_ident + "_netmask"],
                    strict=False,
                )

                if (
                    ipaddress.ip_address(template_opts["network_general_private_gw"])
                    in internal_network
                ):
                    net_iface["network_iface_to_private_gw"] = True
                else:
                    net_iface["network_iface_to_private_gw"] = False

            lookup_vars(
                template_opts, net_iface, key_ident + "_mode", "network_iface_bat_mode"
            )
            lookup_vars(
                template_opts,
                net_iface,
                key_ident + "_mode6",
                "network_iface_bat_mode6",
            )
            lookup_vars(
                template_opts, net_iface, key_ident + "_ip", "network_iface_bat_ip"
            )
            lookup_vars(
                template_opts,
                net_iface,
                key_ident + "_netmask",
                "network_iface_bat_netmask",
            )
            lookup_vars(
                template_opts,
                net_iface,
                key_ident + "_hwaddr",
                "network_iface_bat_hwaddr",
            )
            lookup_vars(
                template_opts, net_iface, key_ident + "_mtu", "network_iface_bat_mtu"
            )
            lookup_vars(
                template_opts, net_iface, key_ident + "_gw", "network_iface_bat_gw"
            )
            lookup_vars(
                template_opts, net_iface, key_ident + "_ptp", "network_iface_bat_ptp"
            )
            lookup_vars(
                template_opts, net_iface, key_ident + "_ip6", "network_iface_bat_ip6"
            )
            lookup_vars(
                template_opts,
                net_iface,
                key_ident + "_netmask6",
                "network_iface_bat_netmask6",
            )
            lookup_vars(
                template_opts, net_iface, key_ident + "_gw6", "network_iface_bat_gw6"
            )
            lookup_vars(
                template_opts, net_iface, key_ident + "_pre_up", "network_iface_pre_up"
            )
            lookup_vars(
                template_opts,
                net_iface,
                key_ident + "_post_up",
                "network_iface_post_up",
            )

            merged_opts = {**template_opts, **net_iface}

            config_template_file = open(
                module_prefix
                + "/templates/config-fs/dynamic/etc/network/interfaces.d/bat.cfg",
                "r",
            ).read()
            config_template = django_engine.from_string(config_template_file)

            zip_add_file(
                zipfile_handler,
                "etc/network/interfaces.d/" + net_iface["network_iface"] + ".cfg",
                clean_new_lines(config_template.render(merged_opts)),
            )

            template_opts["single_network_file_content"] += (
                clean_new_lines(config_template.render(merged_opts)) + "\n\n"
            )

            if "network_iface_pre_up" in merged_opts:
                config_template_file = open(
                    module_prefix
                    + "/templates/config-fs/dynamic/etc/network/scripts/pre-up.sh",
                    "r",
                ).read()
                config_template = django_engine.from_string(config_template_file)

                zip_add_file(
                    zipfile_handler,
                    "etc/network/scripts/" + network_iface + "/pre-up.sh",
                    clean_new_lines(config_template.render(merged_opts)),
                )

            if "network_iface_post_up" in merged_opts:
                config_template_file = open(
                    module_prefix
                    + "/templates/config-fs/dynamic/etc/network/scripts/post-up.sh",
                    "r",
                ).read()
                config_template = django_engine.from_string(config_template_file)

                zip_add_file(
                    zipfile_handler,
                    "etc/network/scripts/" + network_iface + "/post-up.sh",
                    clean_new_lines(config_template.render(merged_opts)),
                )

        if key.startswith("network_iface_wlan") and key.endswith("_hwaddr"):
            key_ident = key[:-7]

            network_iface_split = key.split("_")
            network_iface = network_iface_split[2]

            net_iface = dict()
            net_iface["network_iface"] = network_iface

            if "network_general_private_gw" in template_opts:
                internal_network = ipaddress.ip_network(
                    template_opts[key_ident + "_ip"]
                    + "/"
                    + template_opts[key_ident + "_netmask"],
                    strict=False,
                )

                if (
                    ipaddress.ip_address(template_opts["network_general_private_gw"])
                    in internal_network
                ):
                    net_iface["network_iface_to_private_gw"] = True
                else:
                    net_iface["network_iface_to_private_gw"] = False

            lookup_vars(
                template_opts, net_iface, key_ident + "_mode", "network_iface_wlan_mode"
            )
            lookup_vars(
                template_opts,
                net_iface,
                key_ident + "_mode6",
                "network_iface_wlan_mode6",
            )
            lookup_vars(
                template_opts, net_iface, key_ident + "_ip", "network_iface_wlan_ip"
            )
            lookup_vars(
                template_opts,
                net_iface,
                key_ident + "_connect_ssid",
                "network_iface_wlan_connect_ssid",
            )
            lookup_vars(
                template_opts,
                net_iface,
                key_ident + "_connect_psk",
                "network_iface_wlan_connect_psk",
            )
            lookup_vars(
                template_opts,
                net_iface,
                key_ident + "_netmask",
                "network_iface_wlan_netmask",
            )
            lookup_vars(
                template_opts,
                net_iface,
                key_ident + "_hwaddr",
                "network_iface_wlan_hwaddr",
            )
            lookup_vars(
                template_opts, net_iface, key_ident + "_mtu", "network_iface_wlan_mtu"
            )
            lookup_vars(
                template_opts, net_iface, key_ident + "_gw", "network_iface_wlan_gw"
            )
            lookup_vars(
                template_opts, net_iface, key_ident + "_ptp", "network_iface_wlan_ptp"
            )
            lookup_vars(
                template_opts, net_iface, key_ident + "_ip6", "network_iface_wlan_ip6"
            )
            lookup_vars(
                template_opts,
                net_iface,
                key_ident + "_netmask6",
                "network_iface_wlan_netmask6",
            )
            lookup_vars(
                template_opts, net_iface, key_ident + "_gw6", "network_iface_wlan_gw6"
            )
            lookup_vars(
                template_opts, net_iface, key_ident + "_pre_up", "network_iface_pre_up"
            )
            lookup_vars(
                template_opts,
                net_iface,
                key_ident + "_post_up",
                "network_iface_post_up",
            )

            merged_opts = {**template_opts, **net_iface}

            config_template_file = open(
                module_prefix
                + "/templates/config-fs/dynamic/etc/network/interfaces.d/wlan.cfg",
                "r",
            ).read()
            config_template = django_engine.from_string(config_template_file)

            zip_add_file(
                zipfile_handler,
                "etc/network/interfaces.d/" + net_iface["network_iface"] + ".cfg",
                clean_new_lines(config_template.render(merged_opts)),
            )

            template_opts["single_network_file_content"] += (
                clean_new_lines(config_template.render(merged_opts)) + "\n\n"
            )

            if "network_iface_pre_up" in merged_opts:
                config_template_file = open(
                    module_prefix
                    + "/templates/config-fs/dynamic/etc/network/scripts/pre-up.sh",
                    "r",
                ).read()
                config_template = django_engine.from_string(config_template_file)

                zip_add_file(
                    zipfile_handler,
                    "etc/network/scripts/" + network_iface + "/pre-up.sh",
                    clean_new_lines(config_template.render(merged_opts)),
                )

            if "network_iface_post_up" in merged_opts:
                config_template_file = open(
                    module_prefix
                    + "/templates/config-fs/dynamic/etc/network/scripts/post-up.sh",
                    "r",
                ).read()
                config_template = django_engine.from_string(config_template_file)

                zip_add_file(
                    zipfile_handler,
                    "etc/network/scripts/" + network_iface + "/post-up.sh",
                    clean_new_lines(config_template.render(merged_opts)),
                )

        if key.startswith("network_iface_gre") and key.endswith("_remote"):
            key_ident = key[:-7]

            network_iface_split = key.split("_")
            network_iface = network_iface_split[2]

            net_iface = dict()
            net_iface["network_iface"] = network_iface

            if "network_general_private_gw" in template_opts:
                internal_network = ipaddress.ip_network(
                    template_opts[key_ident + "_ip"]
                    + "/"
                    + template_opts[key_ident + "_netmask"],
                    strict=False,
                )

                if (
                    ipaddress.ip_address(template_opts["network_general_private_gw"])
                    in internal_network
                ):
                    net_iface["network_iface_to_private_gw"] = True
                else:
                    net_iface["network_iface_to_private_gw"] = False

            lookup_vars(
                template_opts, net_iface, key_ident + "_mode", "network_iface_gre_mode"
            )
            lookup_vars(
                template_opts,
                net_iface,
                key_ident + "_mode6",
                "network_iface_gre_mode6",
            )
            lookup_vars(
                template_opts, net_iface, key_ident + "_ip", "network_iface_gre_ip"
            )
            lookup_vars(
                template_opts,
                net_iface,
                key_ident + "_netmask",
                "network_iface_gre_netmask",
            )
            lookup_vars(
                template_opts, net_iface, key_ident + "_mtu", "network_iface_gre_mtu"
            )
            lookup_vars(
                template_opts, net_iface, key_ident + "_ip6", "network_iface_gre_ip6"
            )
            lookup_vars(
                template_opts,
                net_iface,
                key_ident + "_netmask6",
                "network_iface_gre_netmask6",
            )
            lookup_vars(
                template_opts, net_iface, key_ident + "_pre_up", "network_iface_pre_up"
            )
            lookup_vars(
                template_opts,
                net_iface,
                key_ident + "_post_up",
                "network_iface_post_up",
            )

            merged_opts = {**template_opts, **net_iface}

            config_template_file = open(
                module_prefix
                + "/templates/config-fs/dynamic/etc/network/interfaces.d/gre.cfg",
                "r",
            ).read()
            config_template = django_engine.from_string(config_template_file)

            zip_add_file(
                zipfile_handler,
                "etc/network/interfaces.d/" + net_iface["network_iface"] + ".cfg",
                clean_new_lines(config_template.render(merged_opts)),
            )

            template_opts["single_network_file_content"] += (
                clean_new_lines(config_template.render(merged_opts)) + "\n\n"
            )

            if "network_iface_pre_up" in merged_opts:
                config_template_file = open(
                    module_prefix
                    + "/templates/config-fs/dynamic/etc/network/scripts/pre-up.sh",
                    "r",
                ).read()
                config_template = django_engine.from_string(config_template_file)

                zip_add_file(
                    zipfile_handler,
                    "etc/network/scripts/" + network_iface + "/pre-up.sh",
                    clean_new_lines(config_template.render(merged_opts)),
                )

            if "network_iface_post_up" in merged_opts:
                config_template_file = open(
                    module_prefix
                    + "/templates/config-fs/dynamic/etc/network/scripts/post-up.sh",
                    "r",
                ).read()
                config_template = django_engine.from_string(config_template_file)

                zip_add_file(
                    zipfile_handler,
                    "etc/network/scripts/" + network_iface + "/post-up.sh",
                    clean_new_lines(config_template.render(merged_opts)),
                )

        if key.startswith("network_iface_ns") and key.endswith("_hwaddr"):
            key_ident = key[:-7]

            network_iface_split = key.split("_")
            network_iface = network_iface_split[2]

            net_iface = dict()
            net_iface["network_iface"] = network_iface

            if "network_general_private_gw" in template_opts:
                internal_network = ipaddress.ip_network(
                    template_opts[key_ident + "_ip"]
                    + "/"
                    + template_opts[key_ident + "_netmask"],
                    strict=False,
                )

                if (
                    ipaddress.ip_address(template_opts["network_general_private_gw"])
                    in internal_network
                ):
                    net_iface["network_iface_to_private_gw"] = True
                else:
                    net_iface["network_iface_to_private_gw"] = False

            lookup_vars(
                template_opts, net_iface, key_ident + "_mode", "network_iface_ns_mode"
            )
            lookup_vars(
                template_opts,
                net_iface,
                key_ident + "_mode6",
                "network_iface_ns_mode6",
            )
            lookup_vars(
                template_opts, net_iface, key_ident + "_ip", "network_iface_ns_ip"
            )
            lookup_vars(
                template_opts,
                net_iface,
                key_ident + "_netmask",
                "network_iface_ns_netmask",
            )
            lookup_vars(
                template_opts, net_iface, key_ident + "_mtu", "network_iface_ns_mtu"
            )
            lookup_vars(
                template_opts, net_iface, key_ident + "_ip6", "network_iface_ns_ip6"
            )
            lookup_vars(
                template_opts,
                net_iface,
                key_ident + "_netmask6",
                "network_iface_ns_netmask6",
            )
            lookup_vars(
                template_opts, net_iface, key_ident + "_pre_up", "network_iface_pre_up"
            )
            lookup_vars(
                template_opts,
                net_iface,
                key_ident + "_post_up",
                "network_iface_post_up",
            )

            # specific settings
            lookup_vars(
                template_opts,
                net_iface,
                key_ident + "_peer_netns",
                "network_iface_ns_peer_netns",
            )

            lookup_vars(
                template_opts,
                net_iface,
                key_ident + "_peer_iface",
                "network_iface_ns_peer_iface",
            )

            lookup_vars(
                template_opts,
                net_iface,
                key_ident + "_veth_mode",
                "network_iface_ns_veth_mode",
            )

            lookup_vars(
                template_opts,
                net_iface,
                key_ident + "_configure_interfaces",
                "network_iface_ns_configure_interfaces",
            )

            merged_opts = {**template_opts, **net_iface}

            config_template_file = open(
                module_prefix
                + "/templates/config-fs/dynamic/etc/network/interfaces.d/ns.cfg",
                "r",
            ).read()
            config_template = django_engine.from_string(config_template_file)

            zip_add_file(
                zipfile_handler,
                "etc/network/interfaces.d/" + net_iface["network_iface"] + ".cfg",
                clean_new_lines(config_template.render(merged_opts)),
            )

            template_opts["single_network_file_content"] += (
                clean_new_lines(config_template.render(merged_opts)) + "\n\n"
            )

            if "network_iface_pre_up" in merged_opts:
                config_template_file = open(
                    module_prefix
                    + "/templates/config-fs/dynamic/etc/network/scripts/pre-up.sh",
                    "r",
                ).read()
                config_template = django_engine.from_string(config_template_file)

                zip_add_file(
                    zipfile_handler,
                    "etc/network/scripts/" + network_iface + "/pre-up.sh",
                    clean_new_lines(config_template.render(merged_opts)),
                )

            if "network_iface_post_up" in merged_opts:
                config_template_file = open(
                    module_prefix
                    + "/templates/config-fs/dynamic/etc/network/scripts/post-up.sh",
                    "r",
                ).read()
                config_template = django_engine.from_string(config_template_file)

                zip_add_file(
                    zipfile_handler,
                    "etc/network/scripts/" + network_iface + "/post-up.sh",
                    clean_new_lines(config_template.render(merged_opts)),
                )

    template_opts["pppoe_connections"] = pppoe_connections

    template_opts["net_ifaces_eth"] = sorted(
        net_ifaces_eth, key=lambda k: k["network_iface"]
    )

    template_opts["network_routes_static_list"] = list()

    template_opts["apt_sources_auth_hosts_list"] = list()

    template_opts["apt_sources_auth_hosts_list"].append(
        "debian.packages.managed-infra.com"
    )
    template_opts["apt_sources_auth_hosts_list"].append(
        "debian-archive.packages.managed-infra.com"
    )
    template_opts["apt_sources_auth_hosts_list"].append(
        "debian-security.packages.managed-infra.com"
    )

    for key in template_opts.keys():
        if key.startswith("network_route_static_") and key.endswith("_subnet"):
            key_ident = key[:-7]

            route_details = dict()

            route_details["subnet"] = template_opts[key_ident + "_subnet"]

            if key_ident + "_gateway" in template_opts:
                route_details["gateway"] = template_opts[key_ident + "_gateway"]

            if key_ident + "_device" in template_opts:
                route_details["device"] = template_opts[key_ident + "_device"]

            template_opts["network_routes_static_list"].append(route_details)

    if "apt_sources" in template_opts:
        for repo_name, apt_source in template_opts["apt_sources"].items():
            repo_content = str()

            for repo in apt_source:
                repo_content += repo + "\n"

                extracted_hostname = extract_hostname(repo)

                if isinstance(extracted_hostname, str):
                    template_opts["apt_sources_auth_hosts_list"].append(
                        extracted_hostname
                    )

            zip_add_file(
                zipfile_handler,
                "etc/apt/sources.list.d/" + repo_name + ".list",
                repo_content,
            )

    template_opts["apt_sources_auth_hosts_list"] = list(
        set(template_opts["apt_sources_auth_hosts_list"])
    )

    if "apt_preferences" in template_opts:
        for pref_name, pref_settings in template_opts["apt_preferences"].items():
            pref_content = str()

            for pref_setting in pref_settings:
                pref_content += "Package: " + str(pref_setting["package"]) + "\n"
                pref_content += "Pin: " + str(pref_setting["pin"]) + "\n"
                pref_content += (
                    "Pin-Priority: " + str(pref_setting["pin-priority"]) + "\n"
                )
                pref_content += "\n"

            zip_add_file(
                zipfile_handler, "etc/apt/preferences.d/" + pref_name, pref_content
            )

    generate_config_static(zipfile_handler, template_opts, module_prefix)
    handle_icinga2_client(zipfile_handler, template_opts, cmdb_host)

    # MOTD
    zip_add_file(
        zipfile_handler,
        "etc/motd",
        generate_motd(cmdb_host, template_opts),
    )

    return True
